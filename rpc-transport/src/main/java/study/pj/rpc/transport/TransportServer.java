package study.pj.rpc.transport;

/**
 * 1、启动监听端口
 * 2、等待客户端后请求（接受请求）
 * 3、关闭监听
 */
public interface TransportServer {
    void init(int port, RequestHandler handler);

    void start();

    void close();
}
