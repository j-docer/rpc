package study.pj.rpc.codec.impl;

import com.alibaba.fastjson.JSON;
import study.pj.rpc.codec.Decoder;

/**
 * 基于JSON的反序列化实现
 */
public class JsonDecoder implements Decoder {
    @Override
    public <T> T decode(byte[] bytes, Class<T> clazz) {
        return JSON.parseObject(bytes, clazz);
    }
}
