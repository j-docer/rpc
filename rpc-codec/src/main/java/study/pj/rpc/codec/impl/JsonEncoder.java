package study.pj.rpc.codec.impl;

import com.alibaba.fastjson.JSON;
import study.pj.rpc.codec.Encoder;

/*
 * 基于JSON的序列化实现
 * */
public class JsonEncoder implements Encoder {
    @Override
    public byte[] encode(Object obj) {
        return JSON.toJSONBytes(obj);
    }
}
