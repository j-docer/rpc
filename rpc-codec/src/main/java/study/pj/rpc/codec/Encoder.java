package study.pj.rpc.codec;

/**
 * 将对象转换为Byte的数组 : 序列化
 */
public interface Encoder {
    byte[] encode(Object obj);
}
