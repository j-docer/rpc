package study.pj.rpc.codec.test;

import org.junit.Assert;
import org.junit.Test;
import study.pj.rpc.codec.Decoder;
import study.pj.rpc.codec.Encoder;
import study.pj.rpc.codec.impl.JsonDecoder;
import study.pj.rpc.codec.impl.JsonEncoder;

/**
 * 单元测试类：测试序列化和反序列化的操作。
 */
public class JsonEncoderTest {
    @Test
    public void encode() {
        Encoder encoder = new JsonEncoder();
        TestBean testBean = new TestBean();
        testBean.setAge(123);
        testBean.setName("asdf");
        byte[] bytes = encoder.encode(testBean);
        System.out.println(bytes);
        Assert.assertNotNull(bytes);
        // 反序列化
        Decoder decoder = new JsonDecoder();
        TestBean te = decoder.decode(bytes, TestBean.class);
//        Assert.assertEquals(te, TestBean.class);
        Assert.assertEquals(te.getAge(), testBean.getAge());
        Assert.assertEquals(te.getName(), testBean.getName());
        System.out.println("end");

    }

}
