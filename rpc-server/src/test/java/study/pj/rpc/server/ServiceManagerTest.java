package study.pj.rpc.server;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import study.pj.rpc.common.utils.ReflectUtils;
import study.pj.rpc.common.utils.proto.Request;
import study.pj.rpc.common.utils.proto.ServiceDescriptor;

import java.lang.reflect.Method;

public class ServiceManagerTest {
    ServiceManager sm;

    @Before
    public void inti() {
        sm = new ServiceManager();
        TestInterface bean = new TestInterfaceImpl();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void register() {
        TestInterface bean = new TestInterfaceImpl();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void lookup() {
        Method method = ReflectUtils.getPublicMethods(TestInterface.class)[0];
        ServiceDescriptor sdp = ServiceDescriptor.from(TestInterface.class, method);
        Request request = new Request();
        request.setService(sdp);
        ServiceInstance sis = sm.lookup(request);
        Assert.assertNotNull(sis);
    }


}
