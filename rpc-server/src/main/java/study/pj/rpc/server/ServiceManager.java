package study.pj.rpc.server;

import study.pj.rpc.common.utils.ReflectUtils;
import study.pj.rpc.common.utils.proto.Request;
import study.pj.rpc.common.utils.proto.ServiceDescriptor;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 管理RPC暴露的服务
 */
public class ServiceManager {

    private Map<ServiceDescriptor, ServiceInstance> servicesMap;

    // 使用并发map的实现。
    public ServiceManager() {
        this.servicesMap = new ConcurrentHashMap<>();
    }

    /**
     * 服务注册，扫描接口实现类中所有的方法和参数，和Bean绑定起来，存放在容器中。
     *
     * @param interfaceClass 接口
     * @param bean           接口的具体实现类
     */
    public <T> void register(Class<T> interfaceClass, T bean) {
        // 将接口中的所有的方法都扫描出来，
        Method[] methods = ReflectUtils.getPublicMethods(interfaceClass);
        for (Method method : methods) {
            ServiceInstance sis = new ServiceInstance(bean, method);
            ServiceDescriptor sdp = ServiceDescriptor.from(interfaceClass, method);
            servicesMap.put(sdp, sis);
            System.out.println("register service{} {}" + sdp.getClazz() + "-" + sdp.getMethod());
        }
    }

    public ServiceInstance lookup(Request request) {
        ServiceDescriptor sdp = request.getService();
        return servicesMap.get(sdp);
    }
}
