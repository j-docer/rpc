package study.pj.rpc.server;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import study.pj.rpc.codec.Decoder;
import study.pj.rpc.codec.Encoder;
import study.pj.rpc.common.utils.ReflectUtils;
import study.pj.rpc.common.utils.proto.Request;
import study.pj.rpc.common.utils.proto.Response;
import study.pj.rpc.transport.RequestHandler;
import study.pj.rpc.transport.TransportServer;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RpcServer {
    private RpcServerConfig config;
    private TransportServer net;
    private Encoder encoder;
    private Decoder decoder;
    private ServiceManager serviceManager;
    private ServiceInvoker serviceInvoker;

    public RpcServer(RpcServerConfig config) {
        this.config = config;
        this.net = ReflectUtils.newInstance(config.getTransportClass());
        this.net.init(config.getPort(),this.handler);
        this.encoder = ReflectUtils.newInstance(config.getEncoderClass());
        this.decoder = ReflectUtils.newInstance(config.getDecoderClass());
        this.serviceManager = new ServiceManager();
        this.serviceInvoker = new ServiceInvoker();
    }

    public <T> void register(Class<T> interfaceClass, T bean) {
        serviceManager.register(interfaceClass, bean);
    }

    public void start() {
        this.net.start();
    }

    public void stop() {
        this.net.close();
    }

    private RequestHandler handler = new RequestHandler() {
        @Override
        public void onRequest(InputStream receive, OutputStream toResp) {
            Response resp = new Response();
            try {
                byte[] inBytes = IOUtils.readFully(receive, receive.available());
                Request request = decoder.decode(inBytes, Request.class);
                System.out.println("get Request:" + request);
                ServiceInstance sis = serviceManager.lookup(request);
                Object ret = serviceInvoker.invoke(sis, request);
                resp.setData(ret);
            } catch (IOException e) {
                e.printStackTrace();
                resp.setCode(1);
                resp.setMessage("Rpc Server got Error" + e.getClass().getName() + "-" + e.getMessage());
            } finally {
                try {
                    byte[] outbytes = encoder.encode(resp);
                    toResp.write(outbytes);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage() + "e:" + e);
                }
            }


        }
    };

}
