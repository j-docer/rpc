package study.pj.rpc.server;

import study.pj.rpc.common.utils.ReflectUtils;
import study.pj.rpc.common.utils.proto.Request;

/**
 * 调用具体服务
 */
public class ServiceInvoker {
    public Object invoke(ServiceInstance serviceInstance, Request request) {
        return ReflectUtils.invoke(serviceInstance.getTarget(), serviceInstance.getMethod()
                , request.getParameters());
    }
}
