package study.pj.rpc.server;

import lombok.Data;
import study.pj.rpc.codec.Decoder;
import study.pj.rpc.codec.Encoder;
import study.pj.rpc.codec.impl.JsonDecoder;
import study.pj.rpc.codec.impl.JsonEncoder;
import study.pj.rpc.transport.TransportServer;
import study.pj.rpc.transport.impl.HttpTransportServer;

@Data
public class RpcServerConfig {

    private Class<? extends TransportServer> transportClass = HttpTransportServer.class;

    // 序列化模块类的实现
    private Class<? extends Encoder> encoderClass = JsonEncoder.class;
    // 反序列化模块类的实现
    private Class<? extends Decoder> decoderClass = JsonDecoder.class;

    private int port = 3000;

}
