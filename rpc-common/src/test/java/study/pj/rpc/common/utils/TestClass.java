package study.pj.rpc.common.utils;

public class TestClass {
    private String a() {
        return "a";
    }

    public String b() {
        return "b";
    }

    protected String c() {
        System.out.println("c");
        return "c";
    }
}
