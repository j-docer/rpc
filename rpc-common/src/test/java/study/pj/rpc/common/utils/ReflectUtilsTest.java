package study.pj.rpc.common.utils;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;

///单元测试
public class ReflectUtilsTest {
    @Test
    public void newInstance() {
        TestClass t = ReflectUtils.newInstance(TestClass.class);
        System.out.println("t" + t);
        Assert.assertNotNull(t);
    }

    @Test
    public void getPublicMethods() {
        Method[] methods = ReflectUtils.getPublicMethods(TestClass.class);
        Assert.assertEquals(1, methods.length);
        String name = methods[0].getName();
        System.out.println(name);
        Assert.assertEquals("b", name);
    }

    @Test
    public void invoke() {
        Method[] methods = ReflectUtils.getPublicMethods(TestClass.class);
        Method b = methods[0];
        TestClass t = new TestClass();
        Object r = ReflectUtils.invoke(t, b);
        Assert.assertEquals("b", r);
    }
}

