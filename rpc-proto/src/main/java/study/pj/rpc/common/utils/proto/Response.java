package study.pj.rpc.common.utils.proto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {
    /*
     * 服务返回编码 o-成功 非0-失败
     */
    private int code;
    /*
     * 具体的错误信息
     */
    private String message;
    /*
     * 返回的数据
     * */
    private Object data;
}

