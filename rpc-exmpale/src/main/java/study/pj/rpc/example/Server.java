package study.pj.rpc.example;

import study.pj.rpc.example.impl.CalServiceImpl;
import study.pj.rpc.server.RpcServer;
import study.pj.rpc.server.RpcServerConfig;

import java.util.ArrayList;
import java.util.List;

public class Server {
    public static void main(String[] args) {
        RpcServerConfig rpcServerConfig = new RpcServerConfig();
        RpcServer server = new RpcServer(rpcServerConfig);
        server.register(CalService.class, new CalServiceImpl());
        server.start();

        List<String> ad = new ArrayList<String>();


    }
}
