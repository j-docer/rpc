package study.pj.rpc.example;

import study.pj.rpc.client.RpcClient;

public class Client {
    public static void main(String[] args) {
        RpcClient client = new RpcClient();
        CalService service = client.getProxy(CalService.class);
        int r1 = service.add(1, 2);
        int r2 = service.minus(3, 4);
        System.out.println(r1 + "-" + r2);

    }
}
