package study.pj.rpc.example;


/**
 * heap space OOM
 */
public class Test {
    final int[] arr = new int[1000000000];

    public void test() {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void main(String[] args) {
        Test test = new Test();
        test.test();
    }
}
