package study.pj.rpc.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import study.pj.rpc.codec.Decoder;
import study.pj.rpc.codec.Encoder;
import study.pj.rpc.common.utils.ReflectUtils;

import java.lang.reflect.Proxy;

@Data
@AllArgsConstructor
public class RpcClient {
    private RpcClientConfig config;
    private Encoder encoder;
    private Decoder decoder;
    private TransportSelector selector;

    public RpcClient() {
        this(new RpcClientConfig());
    }

    public RpcClient(RpcClientConfig config) {
        this.config = config;

        this.encoder = ReflectUtils.newInstance(this.config.getEncoderClass());
        this.decoder = ReflectUtils.newInstance(this.config.getDecoderClass());
        this.selector = ReflectUtils.newInstance(this.config.getSelectorClass());

        this.selector.init(this.config.getServers(),
                this.config.getConnectCount(),
                this.config.getTransportClass());
    }

    public <T> T getProxy(Class<T> clazz) {
        return (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{clazz}, new RemoteInvoker(clazz, encoder, decoder, selector));
    }
}

