package study.pj.rpc.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import study.pj.rpc.client.impl.RandomTransportSelector;
import study.pj.rpc.codec.Decoder;
import study.pj.rpc.codec.Encoder;
import study.pj.rpc.codec.impl.JsonDecoder;
import study.pj.rpc.codec.impl.JsonEncoder;
import study.pj.rpc.common.utils.proto.Peer;
import study.pj.rpc.transport.TransportClient;
import study.pj.rpc.transport.impl.HttpTransportClient;

import java.util.Arrays;
import java.util.List;

@Data
public class RpcClientConfig {

    // 配置使用那种网络连接
    private Class<? extends TransportClient> transportClass = HttpTransportClient.class;

    // 配置序列化类
    private Class<? extends Encoder> encoderClass = JsonEncoder.class;
    // 配置反序列化类
    private Class<? extends Decoder> decoderClass = JsonDecoder.class;

    //
    private Class<? extends TransportSelector> selectorClass =
            RandomTransportSelector.class;
    private int connectCount = 1;

    // 默认的服务器连接。
    private List<Peer> servers = Arrays.asList(new Peer("127.0.0.1", 3000));
}
