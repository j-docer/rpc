package study.pj.rpc.client;

import org.apache.commons.io.IOUtils;
import study.pj.rpc.codec.Decoder;
import study.pj.rpc.codec.Encoder;
import study.pj.rpc.common.utils.proto.Request;
import study.pj.rpc.common.utils.proto.Response;
import study.pj.rpc.common.utils.proto.ServiceDescriptor;
import study.pj.rpc.transport.TransportClient;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/*
 *
 * 调用远程的服务的代理类
 *
 * */
public class RemoteInvoker implements InvocationHandler {
    private Class clazz;
    private Encoder encoder;
    private Decoder decoder;
    private TransportSelector selector;

    RemoteInvoker(Class clazz, Encoder encoder, Decoder decoder, TransportSelector selector) {
        this.clazz = clazz;
        this.encoder = encoder;
        this.decoder = decoder;
        this.selector = selector;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Request request = new Request();

        request.setService(ServiceDescriptor.from(clazz, method));
        request.setParameters(args);
        Response resp = invokeRemote(request);

        if (resp.getCode() != 0) {
            // 调用失败了。
            throw new IllegalStateException("fail to invoke remote:" + resp.getMessage());
        }
        return resp.getData();
    }

    private Response invokeRemote(Request request) throws IOException {
        Response resp;
        TransportClient client = null;
        try {
            client = selector.select();
            byte[] outbytes = encoder.encode(request);
            InputStream receive = client.write(new ByteArrayInputStream(outbytes));
            byte[] inBytes = IOUtils.readFully(receive, receive.available());
            resp = decoder.decode(inBytes, Response.class);
        } catch (IOException e) {
            e.printStackTrace();
            resp = new Response();
            resp.setCode(1);
            resp.setMessage("RpeClient  get error:" + e.getMessage());
        } finally {
            if (client != null) {
                selector.release(client);
            }
        }
        return resp;
    }
}
