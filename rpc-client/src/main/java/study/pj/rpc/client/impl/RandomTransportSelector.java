package study.pj.rpc.client.impl;

import study.pj.rpc.client.TransportSelector;
import study.pj.rpc.common.utils.ReflectUtils;
import study.pj.rpc.common.utils.proto.Peer;
import study.pj.rpc.transport.TransportClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomTransportSelector implements TransportSelector {
    /**
     * 连接好的client
     */
    private List<TransportClient> clients;

    public RandomTransportSelector() {
        clients = new ArrayList<>();
    }

    @Override
    public synchronized void init(List<Peer> peerList, int count, Class<? extends TransportClient> clazz) {
        count = Math.max(count, 1);
        for (Peer peer : peerList) {
            for (int i = 0; i < count; i++) {
                TransportClient client = ReflectUtils.newInstance(clazz);
                client.connect(peer);
                clients.add(client);
            }
            System.out.println("connect to server " + peer);
        }
    }

    @Override
    public synchronized TransportClient select() {
        int i = new Random().nextInt(clients.size());
        return clients.remove(i);
    }

    @Override
    public synchronized void release(TransportClient client) {
        clients.add(client);
    }

    @Override
    public synchronized void close() {
        for (TransportClient client : clients) {
            client.close();
        }
        clients.clear();
    }
}
