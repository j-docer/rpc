package study.pj.rpc.client;

import study.pj.rpc.common.utils.proto.Peer;
import study.pj.rpc.transport.TransportClient;

import java.util.List;

/**
 * 表示选择哪个server去连接
 */
public interface TransportSelector {

    /**
     * 初始化连接的方法
     *
     * @Param peerList 可以连接的server端点
     * @Param count client与server建立多少个连接
     * @Param client实现class
     */
    void init(List<Peer> peerList, int count, Class<? extends TransportClient> clazz);

    // 选择一个transport与server交互。
    TransportClient select();

    // 释放用完的client。
    void release(TransportClient client);

    void close();
}
